$(function(){
  //VERIFICA SE O USUARIO TA LOGADO
  if(localStorage.getItem("logado")){
    $("#login").addClass("inativo");
    $("#cadastrar").addClass("inativo");
    $("#opcoes-logado").addClass("ativo");
    $(".form-group").removeClass("inativo");
    if(localStorage.getItem("camera")){
      $('#camera').removeClass("inativo");
      $("#qtd-camera").val(localStorage.getItem("qtd-camera"));
      $("#preco-camera").val(localStorage.getItem("preco-camera"));
      $('.camera-detalhes').append($("<p> Preço: " +  localStorage.getItem("preco-camera")+"R$</p>"));
      $('.camera-detalhes').append($("<p> Qtd: " +  localStorage.getItem("qtd-camera")+"</p>"));

    }
    if(localStorage.getItem("cerveja")){
      $('#cerveja').removeClass("inativo");
      $("#qtd-cerveja").val(localStorage.getItem("qtd-cerveja"));
      $("#preco-cerveja").val(localStorage.getItem("preco-cerveja"));
      $('.cerveja-detalhes').append($("<p> Preço: " +  localStorage.getItem("preco-cerveja")+"R$</p>"));
      $('.cerveja-detalhes').append($("<p> Qtd: " +  localStorage.getItem("qtd-cerveja")+"</p>"));
    }
    if(localStorage.getItem("pneu")){
      $('#pneu').removeClass("inativo");
      $("#qtd-pneu").val(localStorage.getItem("qtd-pneu"));
      $("#preco-pneu").val(localStorage.getItem("preco-pneu"));
      $('.pneu-detalhes').append($("<p> Preço: " +  localStorage.getItem("preco-pneu")+"R$</p>"));
      $('.pneu-detalhes').append($("<p> Qtd: " +  localStorage.getItem("qtd-pneu")+"</p>"));
    }
    if(localStorage.getItem("ype")){
      $('#ype').removeClass("inativo");
      $("#qtd-ype").val(localStorage.getItem("qtd-ype"));
      $("#preco-ype").val(localStorage.getItem("preco-ype"));
      $('.ype-detalhes').append($("<p> Preço: " +  localStorage.getItem("preco-ype")+"R$</p>"));
      $('.ype-detalhes').append($("<p> Qtd: " +  localStorage.getItem("qtd-ype")+"</p>"));
    }
  }

  //SAIR
  $("#logoff").click("click",function(){
    localStorage.clear();
    $("#login").addClass("ativo");
    $("#cadastrar").addClass("ativo");
    $("#opcoes-logado").addClass("inativo");
    $(".form-group").removeClass("ativo");
    location.reload();
  });
  //FIM SAIR
  $(".close").click(function(){
      alert("clickou");
      $(this).hide();
  });
  //CADASTRAR
  document.forms["form-cadastrar"].addEventListener("submit",cadastrar);
  function cadastrar(){
    event.preventDefault();

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState==4 && this.status==200){
            //var divAlert = $("#alert");
            console.log("Funfou!");
            console.log(this.responseText);
            document.forms["form-cadastrar"]["emailCadastrar"].value = "";
            document.forms["form-cadastrar"]["senhaCadastrar"].value = "";
            document.forms["form-cadastrar"]["resenhaCadastrar"].value = "";

            //add feedback sucesso
            var divAlertSuccess = document.createElement('div');
            divAlertSuccess.className = "alert alert-success";
            divAlertSuccess.innerHTML = "Cadastro feito";
            var span = document.createElement('span');
            span.className = "close";
            span.innerHTML = "&times;"

            divAlertSuccess.append(span);
            $("#alert").append(divAlertSuccess);
        }else if( this.status== 400 || this.status== 500){
          //feddbakck erro
          var divAlertSuccess = document.createElement('div');
          divAlertSuccess.className = "alert alert-danger";
          divAlertSuccess.innerHTML = "ERRO";
          var span = document.createElement('span');
          span.className = "close";
          span.innerHTML = "&times;"

          divAlertSuccess.append(span);
          $("#alert").append(divAlertSuccess);
        }
    }
    xhttp.open("POST","http://rest.learncode.academy/api/lucas/teste",true);
    xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");

    //verificar se senhas estão iguais e verificar se ja existe usuário
    var body = "email=" + document.forms["form-cadastrar"]["emailCadastrar"].value
                + "&senha=" + document.forms["form-cadastrar"]["senhaCadastrar"].value;
    xhttp.send(body);
  }
  //FIM CADASTAR
  //LOGAR
    $(".logar").click("click", function(){

    var email = document.forms["form-entrar"]["email"].value;
    var senha = document.forms["form-entrar"]["senha"].value;
    var existe = false;
    $.ajax({
      type: "GET",
      url: "http://rest.learncode.academy/api/lucas/teste",
      success: function(data) {
        var user;
        for(var i = 0; i < data.length; i++){
          console.log(data[i].email);
          if(data[i].email == email && data[i].senha == senha){
            console.log("ENTROU NO IF");
            existe = true;
            user = data[i];
          }
        }
        if(existe){
          console.log("teste");
          $("#login").addClass("inativo");
          $("#cadastrar").addClass("inativo");
          $("#opcoes-logado").addClass("ativo");
          $(".form-group").removeClass("inativo");
          localStorage.setItem("email", user.email);
          localStorage.setItem("logado", true);
          localStorage.setItem("idUsuario", user.id);
        }else{
          var divAlertSuccess = document.createElement('div');
          divAlertSuccess.className = "alert alert-danger";
          divAlertSuccess.innerHTML = "Email ou senha errado";
          var span = document.createElement('span');
          span.className = "close";
          span.innerHTML = "&times;"

          divAlertSuccess.append(span);
          $("#alert").append(divAlertSuccess);
        }
      }
    });

  });
  //FIM LOGAR

  //ADD NO CARRINHO
  // $('.addCamera').click("click", function(){
  //
  //   localStorage.setItem("camera",true);
  // });
  document.forms["form-camera"].addEventListener("submit",addCamera);
  function addCamera(){
    var id = document.forms["form-camera"]["id"].value;
    var preco = document.forms["form-camera"]["preco"].value;
    var qtd = document.forms["form-camera"]["quantidade"].value;
    preco = parseFloat(preco);
    qtd = parseInt(qtd);
    var valorTotal = preco*qtd;

    var qtdCarrinho = $("#qtd-camera").val();
    qtdCarrinho = parseInt(qtdCarrinho);
    var precoCarrinho = $("#preco-camera").val();
    precoCarrinho = parseFloat(precoCarrinho);
    if(qtdCarrinho != 0 && precoCarrinho!= 0){
      valorTotal = valorTotal + precoCarrinho;
      qtdCarrinho = qtdCarrinho + qtd;

      localStorage.setItem("qtd-camera", parseInt(qtdCarrinho));
      localStorage.setItem("preco-camera", valorTotal);
    }else{
      localStorage.setItem("qtd-camera", qtd);
      localStorage.setItem("preco-camera", valorTotal);
    }

    localStorage.setItem("camera",true);

  }

  document.forms["form-cerveja"].addEventListener("submit",addCerveja);
  function addCerveja(){
    var id = document.forms["form-cerveja"]["id"].value;
    var preco = document.forms["form-cerveja"]["preco"].value;
    var qtd = document.forms["form-cerveja"]["quantidade"].value;
    preco = parseFloat(preco);
    qtd = parseInt(qtd);
    var valorTotal = preco*qtd;

    var qtdCarrinho = $("#qtd-cerveja").val();
    qtdCarrinho = parseInt(qtdCarrinho);
    var precoCarrinho = $("#preco-cerveja").val();
    precoCarrinho = parseFloat(precoCarrinho);
    if(qtdCarrinho != 0 && precoCarrinho!= 0){
      valorTotal = valorTotal + precoCarrinho;
      qtdCarrinho = qtdCarrinho + qtd;

      localStorage.setItem("qtd-cerveja", parseInt(qtdCarrinho));
      localStorage.setItem("preco-cerveja", parseFloat(valorTotal));
    }else{
      localStorage.setItem("qtd-cerveja", qtd);
      localStorage.setItem("preco-cerveja", valorTotal);
    }
    localStorage.setItem("cerveja",true);
  }

  document.forms["form-pneu"].addEventListener("submit",addPneu);
  function addPneu(){
    var id = document.forms["form-pneu"]["id"].value;
    var preco = document.forms["form-pneu"]["preco"].value;
    var qtd = document.forms["form-pneu"]["quantidade"].value;
    preco = parseFloat(preco);
    qtd = parseInt(qtd);
    var valorTotal = preco*qtd;

    var qtdCarrinho = $("#qtd-pneu").val();
    qtdCarrinho = parseInt(qtdCarrinho);
    var precoCarrinho = $("#preco-pneu").val();
    precoCarrinho = parseFloat(precoCarrinho);
    if(qtdCarrinho != 0 && precoCarrinho!= 0){
      valorTotal = valorTotal + precoCarrinho;
      qtdCarrinho = qtdCarrinho + qtd;

      localStorage.setItem("qtd-pneu", parseInt(qtdCarrinho));
      localStorage.setItem("preco-pneu", valorTotal);
    }else{
      localStorage.setItem("qtd-pneu", qtd);
      localStorage.setItem("preco-pneu", valorTotal);
    }
    localStorage.setItem("pneu",true);
  }

  document.forms["form-ype"].addEventListener("submit",addYpe);
  function addYpe(){
    var id = document.forms["form-ype"]["id"].value;
    var preco = document.forms["form-ype"]["preco"].value;
    var qtd = document.forms["form-ype"]["quantidade"].value;
    preco = parseFloat(preco);
    qtd = parseInt(qtd);
    var valorTotal = preco*qtd;

    var qtdCarrinho = $("#qtd-ype").val();
    qtdCarrinho = parseInt(qtdCarrinho);
    var precoCarrinho = $("#preco-ype").val();
    precoCarrinho = parseFloat(precoCarrinho);
    if(qtdCarrinho != 0 && precoCarrinho!= 0){
      valorTotal = valorTotal + precoCarrinho;
      qtdCarrinho = qtdCarrinho + qtd;

      localStorage.setItem("qtd-ype", parseInt(qtdCarrinho));
      localStorage.setItem("preco-ype", valorTotal);
    }else{
      localStorage.setItem("qtd-ype", qtd);
      localStorage.setItem("preco-ype", valorTotal);
    }
    localStorage.setItem("ype",true);
  }

  // $(".addPneu").click(function(){
  //   var preco = document.forms["form-pneu"]["preco"].value;
  //   var qtd = document.forms["form-pneu"]["quantidade"].value;
  //   var idProduto = document.forms["form-pneu"]["id"].value;
  //   alert(preco + " " + qtd + " " + id);
  // });
  //
  // $(".addYpe").click(function(){
  //   var preco = document.forms["form-ype"]["preco"].value;
  //   var qtd = document.forms["form-ype"]["quantidade"].value;
  //   var idProduto = document.forms["form-ype"]["id"].value;
  //   alert(preco + " " + qtd + " " + id);
  // });
  //
  // $(".addCerveja").click(function(){
  //   var preco = document.forms["form-cerveja"]["preco"].value;
  //   var qtd = document.forms["form-cerveja"]["quantidade"].value;
  //   var idProduto = document.forms["form-cerveja"]["id"].value;
  //   alert(preco + " " + qtd + " " + id);
  // });
  // FIM ADD NO CARRINHO

  //FINALIZAR COMPRA
  //document.forms["finalizar-compra"].addEventListener("submit",finalizarCompra);

  $(".finalizar").click("click", function(){
    event.preventDefault();

    var idsProduto = "";
    var precoTotal;

    var qtdCamera = document.forms["finalizar-compra"]["qtd-camera"].value;
    var precoCamera = document.forms["finalizar-compra"]["preco-camera"].value;
    qtdCamera = parseInt(qtdCamera);
    precoCamera = parseFloat(precoCamera);
    precoCamera = precoCamera*qtdCamera;
    idsProduto = ids(idsProduto, "1", precoCamera);

    var qtdCerveja = document.forms["finalizar-compra"]["qtd-cerveja"].value;
    var precoCerveja = document.forms["finalizar-compra"]["preco-cerveja"].value;
    qtdCerveja = parseInt(qtdCerveja);
    precoCerveja = parseFloat(precoCerveja);
    precoCerveja = precoCerveja*qtdCerveja;
    idsProduto = ids(idsProduto, "2", precoCerveja);

    var qtdPneu = document.forms["finalizar-compra"]["qtd-pneu"].value;
    var precoPneu = document.forms["finalizar-compra"]["preco-pneu"].value;
    qtdPneu = parseInt(qtdPneu);
    precoPneu = parseFloat(precoPneu);
    precoPneu = precoPneu*qtdPneu;
    idsProduto = ids(idsProduto, "3", precoPneu);

    var qtdYpe = document.forms["finalizar-compra"]["qtd-ype"].value;
    var precoYpe = document.forms["finalizar-compra"]["preco-ype"].value;
    qtdYpe = parseInt(qtdYpe);
    precoYpe = parseFloat(precoYpe);
    precoYpe = precoYpe*qtdYpe;
    idsProduto = ids(idsProduto, "4", precoYpe);

    precoTotal = precoCamera + precoYpe + precoCerveja + precoPneu;


    $.ajax({
      type: 'POST',
      url: 'http://rest.learncode.academy/api/lucas/testeCadastrarProduto',
      data: {
        idComprador: localStorage.getItem("idUsuario"),
        data: "22/01/2016",
        preco: precoTotal,
        idsProduto: idsProduto
      },
      success: function(data) {
        window.location.replace("file:///home/lucas/git/lms_lucassales/Trabalho%203/Bootstrap/suasCompras.html");
      }
    });

  });

  function ids(idsProduto, id, valor){
    if(valor > 1){
      idsProduto = idsProduto + "," + id;
      return idsProduto;
    }
  }


  //FIM FINALIZAR COMPRA

  //MINHAS COMPRAS

  //FIM MINHAS COMPRAS
  //NOvA TESTE



});
